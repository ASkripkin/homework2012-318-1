#include "long.h"
#include <iostream>

/**
 * Code of Long class
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */

Long::Long()
{
    size=0;
    sign=false;
};

std::ostream & operator <<(std::ostream & os,Long &a)
    {
        if (a.size == 0) { os << '0'; return os; }
        if (a.sign == 1) os << '-';
        for (int i=0; i < a.size; i++)
        os << a.val[i];
        return os;
    }

std::istream & operator >>(std::istream & is, Long &a)
    {
        char c;
        string s;
        is >> s;
        int i=0;
        c = s[0];
        if (c == '-') {a.sign = 1; c = s[1]; i++;}
        while (1)
            {
                a.val.push_back(c);
                a.size++;
                i++;
                if ((c<'0')||(c>'9')) {cout<<"Wrong number"<<endl; return is;}
                c = s[i];
                if (!s[i]) {
                                if ((a.size==1)&&(a.val[0]=='0'))
                                {
                                    a.size=0; a.sign = 0;
                                }
                                    return is;

                            }
            }
    }


vector <char> Long::reverse (vector <char> a, int s)
    {
        char c;
        for (int i=0; i<s/2; i++) {
                                    c=a[i];
                                    a[i]=a[s-i-1];
                                    a[s-i-1]=c;
                                  }
        return a;
    };

Long Long::int_To_Long (int a)
    {
        int i=0;
        Long temp;
        if (a<0) {a = -a; temp.sign++;}
        while (a>9)
            {
                temp.val.push_back(a%10 + '0');
                i++;
                a /= 10;
            }
        temp.val.push_back(a + '0');
        i++;
        temp.size = i;
        temp.val = reverse (temp.val, i);
        return temp;
    }

bool Long::operator== (Long a)
    {
        if (this->sign != a.sign) return 0;
        else if (this->size != a.size) return 0;
        else for (int i=0; i < a.size; i++)
                {
                 if (this->val[i] != a.val[i]) return 0;
                }
        return 1;
    }

bool Long::operator!= (Long a)
    {
        if (*this == a) return 0;
        return 1;
    }

bool Long::operator> (Long a)
    {
        if (this->sign < a.sign) return 1;
        else if (this->sign > a.sign) return 0;
        else if (this->size > a.size) return 1-sign;
        else if (this->size < a.size) return sign;
        else for (int i=0; i < a.size; i++)
                {
                 if (this->val[i] < a.val[i]) return sign;
                 else if (this->val[i] > a.val[i]) return 1-sign;
                }
        return 0;
    }

bool Long::operator< (Long a)
    {
        if (this->sign < a.sign) return 0;
        else if (this->sign > a.sign) return 1;
        if (size > a.size) return sign;
        else if (size < a.size) return 1-sign;
        else for (int i=0; i < a.size; i++)
                {
                 if (val[i] < a.val[i]) return 1-sign;
                 else if (val[i] > a.val[i]) return sign;
                }
        return 0;
    }

bool Long::operator>= (Long a)
    {
        if (*this<a) return 0;
        return 1;
    }

bool Long::operator<= (Long a)
    {
        if (*this>a) return 0;
        return 1;
    }

Long Long::plus (Long a1, Long a)
{    
    bool ok = false;
    int sm, sl, i, ai = 0;
    vector <char> strm;
    vector <char> strl;
    Long temp;
    temp.sign = a1.sign;

    if (a1.size < a.size)
            {
                sm = a.size;
                sl = a1.size;
                strm = reverse (a.val, sm);
                strl = reverse (a1.val, sl);
                temp.size = sm;
            }
    else
            {
                sl = a.size;
                sm = a1.size;
                strl = reverse (a.val, sl);
                strm = reverse (a1.val, sm);
                temp.size = sm;
            }


    for (i=0; i < sl; i++)
            {
                ai = strm[i] - '0' +strl[i] - '0' + ok;
                if (ai >= 10)
                        {
                            strm[i]= ai + '0' - 10;
                            ok = 1;
                        }
                else
                        {
                            strm[i]= ai + '0';
                            ok = 0;
                        }
            }


    for (i=sl; i<sm; i++)
            {
                if (strm[i] - '0' + ok >= 10) strm [i] = '0';
                else {
                        strm[i] += ok;
                        ok = 0;
                     }
            }

    if (ok==1)
            {
                ok = 0;
                strm.push_back(1);
                strm[sm] = '1';
                temp.size++;
            }

    temp.val = reverse (strm, temp.size);

    return temp;

}




Long Long::minus (Long a1, Long a)
{
    bool ok = false;
    int sm = a1.size, sl = a.size, i, ai = 0;
    vector <char> strm = reverse (a1.val, sm);
    vector <char> strl = reverse (a.val, sl);
    Long temp;
    temp.size = sm;
    temp.sign = a1.sign;
    for (i=0; i < sl; i++)
            {

                ai = strm[i] - strl[i] - ok;
                if (ai < 0)
                        {
                            strm[i]= ai + '0' + 10;
                            ok = 1;
                        }
                else
                        {
                            strm[i]= ai + '0';
                            ok = 0;
                        }
            }

    for (i=sl; i<sm; i++)
            {
                if (strm[i] - '0' - ok < 0) strm [i] = '9';
                else {
                        strm[i] -= ok;
                        ok = 0;
                     }
            }

    if (strm[sm-1]=='0')
            {
                temp.size--;
            }
    temp.val = reverse (strm, temp.size);

    return temp;

}

Long Long::operator+ (Long a)
{
    if ((this->sign == 0)&&(a.sign == 0))  return (plus (*this,a));

    if ((this->sign == 0)&&(a.sign == 1))
       {
        if (*this < a) return (minus (a,*this)); else
            if (*this == a) {a.sign = 0; a.size = 0; a.val[0] = '0'; return a;}
        else return (minus (*this,a));
       }

    if ((this->sign == 1)&&(a.sign == 0))
       {
        if (*this < a) return (minus (a,*this)); else
            if (*this == a) {a.sign = 0; a.size = 0; a.val[0] = '0'; return a;}
        else return (minus (*this,a));
       }


    if ((this->sign == 1)&&(a.sign == 1))  return (plus (*this,a));
}

Long Long::operator- (Long a)
{
    if ((this->sign == 0)&&(a.sign == 0))
        {
            if (*this < a) {a.sign=1; return (minus (a,*this));} else
                if (*this == a) {a.sign = 0; a.size = 0; a.val[0] = '0'; return a;}
            else return (minus (*this,a));
        }
    if ((this->sign == 0)&&(a.sign == 1))  return (plus (*this,a));

    if ((this->sign == 1)&&(a.sign == 0))
        {this->sign = 0; return (plus (*this,a));}

    if ((this->sign == 1)&&(a.sign == 1))
        {
            if (*this < a) {a.sign=0; return (minus (a,*this));} else
                if (*this == a) {a.sign = 0; a.size = 0; a.val[0] = '0'; return a;}
            else return (minus (*this,a));
        }
}

Long Long::operator* (Long a)
{
    int sm = size, sl = a.size, i, j, ai = 0, ov = 0, op1, op2;
    vector <char> strm = reverse (val, size);
    vector <char> strl = reverse (a.val, a.size);
    Long temp;
    if ((this->sign + a.sign) == 1) temp.sign++;

    for (i=0; i < sl + sm; i++)
        temp.val.push_back('0');
    temp.size = sm + sl;


    for (i=0; i < sl; i++)
            {
                op1 = strl[i] - '0';
                for (j=0; j < sm; j++)
                {
                    op2 = strm[j] - '0';
                    ai = op1*op2 + temp.val[i+j] - '0' + ov;
                    ov = ai / 10;
                    temp.val[i+j] = ai % 10 + '0';
                }
                temp.val[i+sm]+= ov;
                ov = 0;\
            }

    bool ok=1;
    while (ok)
        {
            if (temp.val[temp.size-1]=='0') temp.size--;
            else ok=0;
        }
    temp.val = reverse (temp.val, temp.size);
    return temp;

}


pair <Long, Long> Long::divmod (Long a1, Long a2)
{
    int sm = a1.size, sl = a2.size, i, j=0, ai=0, k=0, l=0;
    bool ok = true;
    bool fl = false;
    pair <Long, Long> temp;
    if ((a1.sign + a2.sign) == 1) temp.first.sign++;
    if (a2.size==0)
                     {
                        cout<<"0 dividing"<<endl;
                        temp.first.val[0] = 'e';
                        temp.second.val[0] = 'e';
                        temp.first.size = 1;
                        temp.second.size = 1;
                        return temp;
                     }
    if (((a1.size==a2.size)&&(a1.val < a2.val)) || (a1.size<a2.size))
                     {
                        temp.first.val.push_back('0');
                        temp.first.size = 1;
                        temp.second.val = a1.val;
                        temp.second.size = sm;
                        return temp;
                     }

    if (a1.val == a2.val)
                    {
                        temp.first.val.push_back('1');
                        temp.first.size = 1;
                        temp.second.val.push_back('0');
                        temp.second.size = 1;
                        temp.first.sign = 0;
                        return temp;
                     }

    vector <char> strm = reverse(a1.val,a1.size);
    vector <char> strl = reverse(a2.val,a2.size);
    vector <char> rez;
    rez.push_back('0');

    while (ok)
        {
            i=0;
            while ((i<sl)&&(ok))
            {
                if (strm [sm-1-i] > strl [sl-1-i]) i=sl;
                else if (strm [sm-1-i] < strl [sl-1-i]) {ok=false; i=sl;}
                i++;
            }

            if ((k>0)&&(!ok)) {strm [sm-1]+=10; ok=true; k--;}

            if (ok)
                {
                    for (j=0; j<sl; j++)
                    {
                        ai = strm [sm-sl+j] - strl [j] - fl;
                        if (ai<0) {strm [sm-sl+j] = ai + '0' + 10; fl = true;}
                        else {strm [sm-sl+j] = ai + '0'; fl = false;}

                    }
                    rez[l]++;
                }


            else
                {
                if (sm>sl)
                    {
                        sm--;
                        k=strm[sm]-'0';
                        rez.push_back('0');
                        ok=true;
                        l++;
                    }

                }
        }
    l++;

    j=0;
    i=0;
    while (i<sm)
        {
            if (strm[sm-1-i]=='0') j++;
            else i=sm-1;
            i++;
        }




    temp.second.val = reverse(strm,sm);
    temp.second.val = reverse(strm,sm-j);


    temp.second.val = reverse(strm,sm-j);
    temp.second.size = sm-j;



    j=0;
    i=0;
    while (i<l)
        {
            if (rez[i]=='0') j++;
            else i=l-1;
            i++;
        }


    for (i=0; i<l-j; i++)
        {
            temp.first.val.push_back(rez[j+i]);
        }

    temp.first.size = l-j;

    if (temp.second.size==0) {temp.second.size++; temp.second.val.push_back('0');}

    return temp;
}

Long Long::operator/ (Long a)
    {
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        pair <Long, Long> t = divmod(newL, a);
        return t.first;
    }

Long Long::operator% (Long a)
    {
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        pair <Long, Long> t = divmod(newL, a);
        return t.second;
    }



Long Long::operator= (Long a)
    {
        Long temp;
        val = a.val;
        size = a.size;
        sign = a.sign;
        return *this;
    }

Long Long::operator+= (Long a)
    {
        *this = *this + a;
        return *this;
    }

Long Long::operator-= (Long a)
    {
        *this = *this - a;
        return *this;
    }

Long Long::operator*= (Long a)
    {
        *this = *this * a;
        return *this;
    }

Long Long::operator/= (Long a)
    {
        *this = *this / a;
        return *this;
    }

Long Long::operator%= (Long a)
    {
        *this = *this % a;
        return *this;
    }


Long Long::operator+ (int a)
    {
        Long temp = int_To_Long(a);
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        newL += temp;
        return newL;
    }

Long Long::operator- (int a)
    {
        Long temp = int_To_Long(a);
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        newL -= temp;
        return newL;
    }

Long Long::operator* (int a)
    {
        Long temp = int_To_Long(a);
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        newL *= temp;
        return newL;
    }

Long Long::operator/ (int a)
    {
        Long temp = int_To_Long(a);
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        newL /= temp;
        return newL;
    }

Long Long::operator% (int a)
    {
        Long temp = int_To_Long(a);
        Long newL;
        newL.val = val;
        newL.size = size;
        newL.sign = sign;
        newL %= temp;
        return newL;
    }

Long Long::operator+= (int a)
    {
        Long temp = int_To_Long(a);
        *this += temp;
        return *this;
    }

Long Long::operator-= (int a)
    {
        Long temp = int_To_Long(a);
        *this -= temp;
        return *this;
    }
Long Long::operator*= (int a)
    {
        Long temp = int_To_Long(a);
        *this *= temp;
        return *this;
    }
Long Long::operator/= (int a)
    {
        Long temp = int_To_Long(a);
        *this /= temp;
        return *this;
    }
Long Long::operator%= (int a)
    {
        Long temp = int_To_Long(a);
        *this += temp;
        return *this;
    }

Long Long::operator= (int a)
    {
        Long temp = int_To_Long(a);
        *this = temp;
        return *this;
    }

bool Long::operator== (int a)
    {
        Long temp = int_To_Long(a);
        if (*this == temp) return 1;
        else return 0;
    }

bool Long::operator!= (int a)
    {
        Long temp = int_To_Long(a);
        if (*this != temp) return 1;
        else return 0;
    }

bool Long::operator> (int a)
    {
        Long temp = int_To_Long(a);
        if (*this > temp) return 1;
        else return 0;
    }

bool Long::operator< (int a)
    {
        Long temp = int_To_Long(a);
        if (*this < temp) return 1;
        else return 0;
    }

bool Long::operator<= (int a)
    {
        Long temp = int_To_Long(a);
        if (*this <= temp) return 1;
        else return 0;
    }

bool Long::operator>= (int a)
    {
        Long temp = int_To_Long(a);
        if (*this >= temp) return 1;
        else return 0;
    }
