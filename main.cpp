/*
 * Copyright (c) 2012 318prak.
 * This program let to user add, divide, multiplate,
 * divide and compare really long numbers.
 */



#include "long_test.h"
#include "long.h"

/**
 * Main function
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */

int main ()

{
    Long a, b, c;
    cin >> a;
    cin >> b;
    cin >> c;
    if (a>=3)
        {
            a += b % c;
            a = a - 165;
            cout<<a;
        }
    else
        {
            a %= c * 3;
            a = a / 4 - 11;
            a *= 7;
            cout<<a;
        }
    return 0;
}
