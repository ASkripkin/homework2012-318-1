.PHONY: clean

all: long.o main.o
	g++ long.o main.o -o all

%.o: %.cpp
	g++ -c $<

long.o: long.h long.cpp
main.o: main.cpp

testall: long-test.h long-test.cpp
	g++ long-test.h long-test.cpp -o testall

clean:
	rm *.o
	rm all
	rm testall