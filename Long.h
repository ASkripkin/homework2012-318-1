#ifndef LONG_H
#define LONG_H
#include <iostream>
#include "long_test.h"
#include "vector"

using namespace std;

/**
 *This class consists of functions and operators that make
 *different actions with very long integer numbers.
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */

class Long
{
    friend class longTest;

public:
    /**
     * Constructor makes long = 0
     */

     Long();
     /**
      * This function reverses digits in number.
      * @param a char array with digits in direct order
      * @param s size of this array
      * @return array with digits in back order
      */
     vector <char> reverse (vector <char> a, int s);

     /**
      * This function divides two "longs".
      * @param a1 divided number
      * @param a2 dividing number
      * @return result pair of parts of rezult number, error if a2=0
      */
     pair <Long, Long> divmod (Long a1, Long a2);

     /**
      * This function converts int in long.
      * @param a1 converted integer
      * @return a in long type
      */
     Long int_To_Long (int a);

     /**
      * This function adds two "longs".
      * @param a1 adding number
      * @param a adding number
      * @return result long
      */
     Long plus (Long a1, Long a);

     /**
      * This function subtracts two "longs".
      * @param a1 subtracting number
      * @param a subtracting number
      * @return result long
      */
     Long minus (Long a1, Long a);

     /**
      * This operator adds two "longs".
      * @param a adding number
      * @return result long
     */
     Long operator+ (Long a);

     /**
      * This operator subtracts two "longs".
      * @param a subtracting number
      * @return result long
     */
     Long operator- (Long a);

     /**
      * This operator multiplys two "longs".
      * @param a multiplying number
      * @return result long
     */
     Long operator* (Long a);

     /**
      * This operator divides two "longs".
      * @param a dividing number
      * @return long - int part of result
      */
     Long operator/ (Long a);

     /**
      * This operator divides two "longs".
      * @param a dividing number
      * @return long - residue of result
      */
     Long operator% (Long a);

     /**
      * This operator adds "long"
      * @param adding number
      * @return long result
     */
     Long operator+= (Long a);

     /**
      * This operator divides "long"
      * @param dividing number
      * @return long result
     */
     Long operator-= (Long a);

     /**
      * This operator multiplyes to "long"
      * @param dividing number
      * @return long result
     */
     Long operator*= (Long a);

     /**
      * This operator divides to "long".
      * @param a dividing number
      * @return long - int part of result
      */
     Long operator/= (Long a);

     /**
      * This operator divides to "long".
      * @param a dividing number
      * @return long - residue of result
      */
     Long operator%= (Long a);

     /**
      * This operator checks if the first "long" greater then the second
      * @param secong number
      * @return true if the first "long" greater then the secong and false in other way
     */
     bool operator> (Long a);

     /**
      * This operator checks if the first "long" less then the second
      * @param secong number
      * @return true if the first "long" less then the secong and false in other way
     */
     bool operator< (Long a);

     /**
      * This operator checks if two "longs" are equal
      * @param secong number
      * @return true if numers are equal and false in other way
     */
     bool operator== (Long a);

     /**
      * This operator checks if two "longs" are equal
      * @param secong number
      * @return false if numers are equal and true in other way
     */
     bool operator!= (Long a);

     /**
      * This operator checks if two "longs" are equal or
      * the first is greater then the second
      * @param secong number
      * @return true if numers are equal and false in other way
     */
     bool operator<= (Long a);

     /**
      * This operator checks if two "longs" are equal or
      * the first is less then the second
      * @param secong number
      * @return true if numers are equal and false in other way
     */
     bool operator>= (Long a);

     /**
      * This operator assigns to the "long" value of another "long"
      * @param another "long"
      * @return Long result - first operand;
     */
     Long operator= (Long a);

     /**
      * This operator adds "long" and int.
      * @param a adding number
      * @return result long
     */
     Long operator+ (int a);

     /**
      * This operator subtracts "long" and int.
      * @param a subtracting number
      * @return result long
     */
     Long operator- (int a);

     /**
      * This operator multiplies "long" and int.
      * @param a multiplying number
      * @return result long
     */
     Long operator* (int a);

     /**
      * This operator divides "long" and int.
      * @param a dividing number
      * @return long - int part of result
      */
     Long operator/ (int a);

     /**
      * This operator divides "long" and int.
      * @param a dividing number
      * @return long - residue of result
      */
     Long operator% (int a);

     /**
      * This operator adds "long" and int
      * @param adding number
      * @return long result
     */
     Long operator+= (int a);

     /**
      * This operator subtracts "long" and int
      * @param subtracting number
      * @return long result
     */
     Long operator-= (int a);

     /**
      * This operator multiplies two "longs".
      * @param multiplying number
      * @return long result
     */
     Long operator*= (int a);

     /**
      * This operator divides "long" to int.
      * @param a dividing int
      * @return long - int part of result
      */
     Long operator/= (int a);

     /**
      * This operator divides "long" to int.
      * @param a dividing int
      * @return long - residue of result
      */
     Long operator%= (int a);

     /**
      * This operator checks if "long" is greater than int
      * @param int number
      * @return true if "long" is greater than int and false in other way
     */
     bool operator> (int a);

     /**
      * This operator checks if "long" is less than int
      * @param int number
      * @return true if "long" is less than int and false in other way
     */
     bool operator< (int a);

     /**
      * This operator checks if "long" is equal to int
      * @param int number
      * @return true if "long" is equal to int and false in other way
     */
     bool operator== (int a);

     /**
      * This operator checks if "long" is equal to int
      * @param int number
      * @return false if "long" is equal to int and true in other way
     */
     bool operator!= (int a);

     /**
      * This operator checks if "long" is equal to int or less
      * @param int number
      * @return false if "long" is equal to int or less and true in other way
     */
     bool operator<= (int a);

     /**
      * This operator checks if "long" is equal to int or greater
      * @param int number
      * @return false if "long" is equal to int or greater and true in other way
     */
     bool operator>= (int a);

     /**
      * This operator assigns to the "long" value of int
      * @param int number
      * @return Long result - "long" operand;
     */
     Long operator= (int a);



     /**
      * This operator enters long number in stream is
      * @param is input stream
      * @param inputing number
     */
     friend std::istream & operator >>(std::istream & is, Long &a);

     /**
      * This operator shows long number from stream os
      * @param os output stream
      * @param outputing number
     */
     friend std::ostream & operator <<(std::ostream & os, Long &a);


     int size;
     bool sign;
     vector<char> val;
private:

};

#endif // LONG_H*
