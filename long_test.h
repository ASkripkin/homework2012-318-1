#ifndef LONG_TEST_H
#define LONG_TEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "long.h"

/**
 * This class consists of test of class Long.
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */

class longTest :
        public CPPUNIT_NS::TestFixture
{
    friend class Long;

    CPPUNIT_TEST_SUITE(longTest);
    CPPUNIT_TEST(testAdd);
    CPPUNIT_TEST(testDec);
    CPPUNIT_TEST(testMul);
    CPPUNIT_TEST(testDiv1);
    CPPUNIT_TEST(testDiv2);
    CPPUNIT_TEST(testEquals);
    CPPUNIT_TEST(testNEquals);
    CPPUNIT_TEST(testGreat);
    CPPUNIT_TEST(testLess);
    CPPUNIT_TEST(testLessEq);
    CPPUNIT_TEST_SUITE_END();
public:
    void setUp();
    void tearDown();
protected:
    /**
     * This function tests addition of two numbers
     * @return true if it works write, false in other way
     */
    void testAdd();

    /**
     * This function tests subtraction of two numbers
     * @return true if it works write, false in other way
     */
    void testDec();

    /**
     * This function tests multiplication of two numbers
     * @return true if it works write, false in other way
     */
    void testMul();

    /**
     * This function tests how program calculate
     * integer part of number, that returns after dividing
     * @return true if it works write, false in other way
     */
    void testDiv1();

    /**
     * This function tests how program calculate
     * residue, that returns after dividing
     * @return true if it works write, false in other way
     */
    void testDiv2();

    /**
     * This function tests comparison of two numbers
     * @return true if it works write, false in other way
     */
    void testEquals();

    /**
     * This function tests write working of operator
     * that finds out if two numbers not equal
     * @return true if it works write, false in other way
     */
    void testNEquals();

    /**
     * This function tests write working of operator
     * that finds out if first long greater than the second
     * @return true if it works write, false in other way
     */
    void testGreat();

    /**
     * This function tests write working of operator
     * that finds out if first long less than the second
     * @return true if it works write, false in other way
     */
    void testLess();

    /**
     * This function tests write working of operator
     * that finds out if first long less or equal to the second
     * @return true if it works write, false in other way
     */
    void testLessEq();

private:

    Long *a, *b, *c;
};
