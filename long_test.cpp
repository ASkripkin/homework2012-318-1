#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "long_test.h"

/**
 * Code of Long_test class
 * @author  Andrew Skripkin
 * @version 1.0
 * @since   1.0
 */

CPPUNIT_TEST_SUITE_REGISTRATION( longTest );
void longTest::setUp()
{
    a= new Long(1);
    b= new Long(1);
    c= new Long(1);
}
void longTest::tearDown()
{
    delete a;
    delete b;
    delete c;
}

void longTest::testAdd()
{
    *a = 90;
    *b = -5;
    *c = 85;
    CPPUNIT_ASSERT(*a + *b == *c);
}


void longTest::testDec()
{
    *a = -100;
    *b =  1;
    *c = -101;
    CPPUNIT_ASSERT(*a - *b == *c);
}



void longTest::testMul()
{
    *a = 100000;
    *b = -9;
    *c = -900000;
    CPPUNIT_ASSERT(*a * *b == *c);
}



void longTest::testDiv1()
 {
    *a = 1000000;
    *b = -100000;
    *c = -10;
    CPPUNIT_ASSERT(*a / *b == *c);
 }



void longTest::testDiv2()
 {
    *a = -10004;
    *b = -1000;
    *c = 4;
    CPPUNIT_ASSERT(*a % *b == *c);
 }

void longTest::testEquals()
{
    *a = -10000;
    *b = -10000;
    CPPUNIT_ASSERT(*a == *b);
}


void longTest::testNEquals()
{
    *a = 5;
    *b = 3;
    CPPUNIT_ASSERT(*a != *b);
}



void longTest::testGreat()
{
    *a = -1500;
    *b = 700;
    CPPUNIT_ASSERT(*a > *b);
}


void longTest::testLess()
{
    *a = 1;
    *b = 2;
    CPPUNIT_ASSERT(*a < *b);
}

void longTest::testLessEq()
{
    *a = -3;
    *b = -3;
    CPPUNIT_ASSERT(*a <= *b);
}

int main( int argc, char **argv)
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry
    = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
    return 0;
}
